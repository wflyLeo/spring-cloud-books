
### **服务发现**
根目录执行

    mvn clean package
    
在target目录下找到spring-cloud-service-discovery-package.zip
放置到本机根目录下解压

    unzip spring-cloud-service-discovery-package.zip

然后进入解压后的bin目录执行start.sh

    cd spring-cloud-service-discovery-package/bin
    ./start.sh

访问http://localhost:8011/discovery/
看到eureka界面
![输入图片说明](https://gitee.com/uploads/images/2018/0415/231132_8d68f67c_43183.png "Snip20180415_2.png")

还可以输入http://localhost:8011/discovery/docs
![输入图片说明](https://gitee.com/uploads/images/2018/0415/231159_104afc58_43183.png "Snip20180415_3.png")
和

http://localhost:8011/discovery/actuator
![输入图片说明](https://gitee.com/uploads/images/2018/0415/231226_595407cf_43183.png "Snip20180415_4.png")

查看相关监控指标，比如搜索框输入info

![输入图片说明](https://gitee.com/uploads/images/2018/0415/231301_22f177ee_43183.png "Snip20180415_5.png")

